'use strict';

class Rabbit {
    constructor(name) {
      this.name = name;
      this.x = 0;
      this.y = 0;
      this.hunters = [];
    }

    on(hunter) {
        this.hunters.push(hunter);
    }

    off(hunter) {
        this.hunters = this.hunters.filter( (cb) => cb !== hunter);
    }
    
    move() {
      this.x = (Math.random() * 1000/2).toFixed(0);
      this.y = (Math.random() * 1000/2).toFixed(0);
      this.hunters.forEach( (cb) => cb(this.x, this.y)); 
      console.log(`Rabbit ${this.name} moved to (${this.x}, ${this.y}).`);
      return jump();
    }
  }
  
class Hunter {
    constructor(name, forest) {
        this.name = name;
        this.x = 0;
        this.y = 0;
        this.xCoord = 0;
        this.yCoord = 0;
        this.watch = this.watch.bind(this);
        this.photo = this.photo.bind(this);
    }

    watch(x, y) {
        this.x = x;
        this.y = y;
        console.log(`${this.name} sees that rabbit Jonny moved to (${this.x}, ${this.y}).`);
    }
   
    photo(rabbit) {
        this.xCoord = (Math.random() * 1000/2).toFixed(0);
        this.yCoord = (Math.random() * 1000/2).toFixed(0);
        let photoSpace = (this.xCoord - rabbit.x)**2 + (this.yCoord - rabbit.y)**2;
        let res = "No photos";
        if (photoSpace <=5000) {
            let win = document.getElementById('winner');
            this.winner = this.name;
            console.log( `${this.name} made new photo with Jonny`);
            this.nameWinner =  document.createTextNode( this.name + " made new photo with Jonny!  " );
            res = `${this.name} made new photo with Jonny`;
            win.appendChild(this.nameWinner);
        }
        console.log(res);
    }
}

class Forest {
    constructor() {
        this.rabbit = new Rabbit("Jonny");
        this.hunters = [];
        this.hunterNum = 1;      
    }

    addHunter() {
        let name = `Hunter ${this.hunterNum++}`
        let hunter = new Hunter(name, this);
        this.hunters.push(hunter);
        this.rabbit.on(hunter.watch);
        return hunter;
    }

    removeHunter(hunter) {
        this.rabbit.off(hunter.watch);
    }

    makePhoto() {
        this.hunters.forEach(hunter => hunter.photo(rabbit));
    }
}

const forest = new Forest();
let rabbit = forest.rabbit;
let moveRabbit = document.getElementById("rabbit");
function jump() {
    moveRabbit.style.top = parseInt(rabbit.x) + 'px';
    moveRabbit.style.left = parseInt(rabbit.y) + 'px';
}

move.addEventListener("click", () => rabbitJump());
function rabbitJump() {
    return rabbit.move();    
}

add.addEventListener("click", () =>addNewHunter());
function addNewHunter() {
    let hunter = forest.addHunter();
    let defaultHunter = document.createElement('div');
    defaultHunter.id = hunter.name;
    defaultHunter.innerHTML = '<div class="hunter">' + 
                                    '<img src="owl.jpg" alt="hunter">' +
                                    '<span class="hunterName">' + defaultHunter.id + '</span>' +
                                    '<div class="rabbitCoords">' +
                                        '<p>Watching for rabbit Jonny...</p>' +
                                    '</div>' +  
                                '</div>';
    huntersContainer.appendChild(defaultHunter);
}   

remove.addEventListener("click", () => removeLastHunter());
function removeLastHunter() {
    let hunter = forest.hunters.pop();
    let hunterID = document.getElementById(hunter.name);
    console.log(hunterID);
    huntersContainer.removeChild(hunterID);
    forest.removeHunter(hunter);
}

makePhoto.addEventListener("click", () => makeNewPhoto());
function makeNewPhoto() {
    return forest.makePhoto();
}
